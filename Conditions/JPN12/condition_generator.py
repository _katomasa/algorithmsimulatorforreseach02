﻿#-*- coding: utf-8 -*-
import datetime
import math
import os
import sys

print("Start generating simulation conditions.")

# pythonの引数を取得.
args = sys.argv
max_additional_hop = 3
link_capacity = int(args[1])
print("link_capacity={}".format(args[1]))

# シミュレーション日を実行日で取得.
simulation_date = datetime.datetime.now().strftime("%Y%m%d")
print("simulation_date={}".format(simulation_date))

# チャネル幅の種類, 最短距離の種類, SQL文の基礎を定義.
bandwidths = [1, 2, 5, 10]
matrixs = [i+1 for i in range(6)]
base_string = "INSERT INTO `algorithm_condition_{simulation_date}`" \
              "(group_id,group_name,algorithm_id,algorithm_name,bandwidth,conditions) " \
              "VALUES ({group_id},'{group_name}',{algo_id},'{algo_name}',{bandwidth},'{conditions}');\n"

# アルゴリズムの条件の種類を定義.
'''
条件の種類を追加する場合はここを追加
'''
groups = [
    {
        "id": 1,
        "name": 'path_priority'
    },
    {
        "id": 2,
        "name": 'additional_hop_count'
    },
    {
        "id": 3,
        "name": 'link_usage'
    },
    {
        "id": 4,
        "name": 'placed_position'
    },

]


# アルゴリズム情報クラスを作成.
class Algo(object):
    id = 0
    name = ""
    base_bandwidth = 0
    base_matrix = 0
    max_value = 0

    def __init__(self, id, name, base_bandwidth, base_matrix, max_value):
        self.id = id
        self.name = name
        self.base_bandwidth = base_bandwidth
        self.base_matrix = base_matrix
        self.max_value = max_value

    def to_dict(self):
        return{
            "id": self.id,
            "name": self.name,
            "base_bandwidth": self.base_bandwidth,
            "base_matrix": self.base_matrix,
            "max_value": self.max_value,
        }

    # 条件出力処理を定義.
    def generate_conditions(self, group, sql_file):
        with open("./conditions/{0}/{1}_{2}.csv".format(simulation_date, group["name"], self.name), 'w', encoding='utf-8') as cond_csv:
            for bandwidth in bandwidths:
                sys.stdout.write("\rGenerate conditions at bandwidth={}".format(bandwidth))
                conditions = []
                for matrix in matrixs:
                    # 基準値との差分から評価値を生成.
                    diff1 = abs(self.base_bandwidth - bandwidth) + 1
                    diff2 = abs(self.base_matrix - matrix) + 1
                    eval_val = diff1 * diff2

                    # 評価値にかける係数を生成.
                    round = math.ceil
                    coefficient = 1
                    if group["name"] == 'additional_hop_count':
                        round = math.floor
                        coefficient = float(max_additional_hop + 1) / float(self.max_value + 1)
                    if group["name"] == 'link_usage':
                        round = math.floor
                        coefficient = 3 / float(self.max_value + 1)
                    if group["name"] == 'placed_position':
                        coefficient = float(link_capacity) / float(self.max_value)

                    value = int(round(eval_val * coefficient))
                    conditions.append(value)
                for i in range(19):
                    conditions.append(0)
                conditions_str = ",".join(map(str, conditions))

                sys.stdout.write("\rAppend SQL Statement.")
                sql_file.write(
                    base_string.format(
                        group_id=group["id"],
                        group_name=group["name"],
                        algo_id=self.id,
                        algo_name=self.name,
                        simulation_date=simulation_date,
                        bandwidth=bandwidth,
                        conditions=conditions_str
                    )
                )
                sys.stdout.write("\rAppend csv statement.")
                cond_csv.write("{}\n".format(conditions_str))
                print("\rGenerated conditions at bandwidth={}".format(bandwidth))

# アルゴリズム情報を作成.
'''
基準値を変更する場合はここを変更する.
max_valueは評価値の最大値.
'''
algorithms = [
    Algo(1, "短距離小容量", 1, 1, 60),
    Algo(2, "短距離中容量", 5, 1, 36),
    Algo(3, "短距離大容量", 10, 1, 60),
    Algo(4, "中距離小容量", 1, 4, 40),
    Algo(5, "中距離中容量", 5, 4, 24),
    Algo(6, "中距離大容量", 10, 4, 40),
    Algo(7, "長距離小容量", 1, 6, 60),
    Algo(8, "長距離中容量", 5, 6, 36),
    Algo(9, "長距離大容量", 10, 6, 60),
]


if __name__ == '__main__':
    if not os.path.exists("./conditions"):
        os.mkdir("./conditions")
        print("Created conditions directory.")
    for group in groups:
        print("group: id={id}, name={name}".format(**group))
        # ディレクトリがない場合は作成.
        if not os.path.exists("./conditions/{0}".format(simulation_date)):
            os.mkdir("./conditions/{0}".format(simulation_date))
            print("Created daily directory.")
        with open("./conditions/{0}/{1}_condition.sql".format(simulation_date, group["name"]), 'w', encoding='utf-8') as f:
            for algorithm in algorithms:
                print("algorithm: id={0.id}, name={0.name}, "
                      "base_bandwidth={0.base_bandwidth}, base_matrix={0.base_matrix}, "
                      "max_value={0.max_value}".format(algorithm))
                algorithm.generate_conditions(group, f)
                print("Generated conditions of algorithm. id={0.id}".format(algorithm))
        print("Generated conditions of group. id={id}".format(**group))
    with open("./conditions/{0}/condition_generator_{0}".format(simulation_date), 'w', encoding='utf-8') as f:
        f.write("simulation_date: {}\n".format(simulation_date))
        f.write("link_capacity: {}\n".format(link_capacity))
        f.write("bandwidths: {}\n".format(bandwidths))
        f.write("matrixs: {}\n".format(matrixs))
        f.write("max_additional_hop: {}\n".format(max_additional_hop))
        f.write("groups:\n")
        [f.write("{}\n".format(group)) for group in groups]
        f.write("algorithms:\n")
        [f.write("{}\n".format(algorithm.to_dict())) for algorithm in algorithms]
        print("Logged generation data.")
    print("Complete generating simulation conditions without errors.")

