select
	group_name as 'グループ名',
	algorithm_id as 'アルゴリズムID',
	algorithm_name as 'アルゴリズム名',
	avg(network_usage) as 'ネットワーク使用率',
	avg(unused_network_resource) as '未使用ネットワーク割合',
	avg(path_bandwidth_usage) as 'パス帯域使用率',
	avg(failed_cnt) as '失敗回数',
	avg(processing_time) as '実行時間'
from
	simulation_result_20161216
group by
	group_id, algorithm_id;