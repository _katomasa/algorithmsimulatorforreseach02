# -*- coding: utf-8 -*-
import csv
from datetime import datetime


def gen_sql_query(algorithm_id, algorithm_name, bandwidth, conditions):
    simulation_date = datetime.now().strftime('%Y%m%d')
    query = "INSERT INTO `algorithm_condition`(group_id,group_name,algorithm_id,algorithm_name,simulation_date,bandwidth,conditions) VALUES (0,'path_priority',{},'{}','{}',{},'{}');\n"
    return query.format(algorithm_id, algorithm_name, simulation_date, bandwidth, conditions)

name_pattern = ["sn","sm","sw","mn","mm","mw","ln","lm","lw"]
algorithm_names = {
    "sn":"Short_Narrow",
    "sm":"Short_Middle",
    "sw":"Short_Wide",
    "mn":"Middle_Narrow",
    "mm":"Middle_Middle",
    "mw":"Middle_Wide",
    "ln":"Long_Narrow",
    "lm":"Long_Middle",
    "lw":"Long_Wide",
    }
bandwidths = [1, 2, 5, 10]

count = 0
with open("path_priority_condition.sql", "w") as sql:
    for file_name in name_pattern:
        count += 1
        with open("path_priority_{}.csv".format(file_name), "r") as f:
            reader = csv.reader(f)
            for row, bandwidth in zip(reader, bandwidths):
                sql.write(gen_sql_query(count, algorithm_names[file_name], bandwidth, ",".join(row)))
        
