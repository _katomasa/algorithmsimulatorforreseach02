﻿using MyDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    public class SimulationData
    {
        /*
        シミュレーション用のデータを集約しておくクラス.
        */
        public readonly int NODE_NUM;
        public readonly int LINK_CAPACITY;

        public const int JPN12_NODE_NUM = 12;
        public const int JPN25_NODE_NUM = 25;

        public const int JPN12_LINK_CAPACITY = 100;
        public const int JPN25_LINK_CAPACITY = 235;

        public const int ALLOW_HOP_MARGIN = 2;

        private readonly DateTime simulationDate;

        public readonly Dictionary<int, List<AlgorithmConditionData>> AlgorithmConditionDict;
        public readonly Dictionary<int, RoutingTable> RoutingTableDict;
        public readonly Dictionary<int, AdjacentNode> AdjacentNodeDict;
        public readonly Dictionary<int, PathBandwidth> PathBandwidthDict;

        public Dictionary<int, Node> Nodes = new Dictionary<int, Node> { };
        public Dictionary<string, Path> Paths = new Dictionary<string, Path> { };
        public Dictionary<string, Link> Links = new Dictionary<string, Link> { };

        public SimulationData(DBManager dbMgr, bool isJPN12)
        {
            if (isJPN12)
            {
                NODE_NUM = JPN12_NODE_NUM;
                LINK_CAPACITY = JPN12_LINK_CAPACITY;
            }
            else
            {
                NODE_NUM = JPN25_NODE_NUM;
                LINK_CAPACITY = JPN25_LINK_CAPACITY;
            }

            simulationDate = DateTime.Now;

            // Loading simulation conditions from database.
            AlgorithmConditionDict = AlgorithmConditionData.RefactorAlgorithmList(AlgorithmCondition.Fetch(dbMgr));
            RoutingTableDict = RoutingTable.Fetch(dbMgr);
            AdjacentNodeDict = AdjacentNode.Fetch(dbMgr);
            PathBandwidthDict = PathBandwidth.Fetch(dbMgr);
            
            // Create simlation data by simulation conditions.
            for (int node1=1; node1<=NODE_NUM; node1++)
            {
                Nodes[node1] = new Node(node1);
                for(int node2=node1+1; node2<NODE_NUM; node2++)
                {
                    Paths[Path.MakeCompositeID(node1, node2)] = new Path(node1, node2, PathBandwidthDict[node1-1].BandwidthIdList[node2-1]);
                    if (AdjacentNodeDict[node1].AdjacentNodeList.Contains(node2))
                    {
                        Links[Link.MakeCompositeID(node1, node2)] = new Link(node1, node2, LINK_CAPACITY);
                    }
                }
            }

            foreach(Node node in Nodes.Values)
            {
                List<List<AdjacentNode>> routeList = new List<List<AdjacentNode>> { new List<AdjacentNode> { AdjacentNodeDict[node.Id] } };
                while (routeList.Count < NODE_NUM)
                {
                    List<AdjacentNode> routeTier = new List<AdjacentNode>();
                    foreach(AdjacentNode adjacentNode in routeList[routeList.Count-1])
                    {
                        foreach(int nodeId in adjacentNode.AdjacentNodeList)
                        {
                            routeTier.Add(AdjacentNodeDict[nodeId]);
                        }
                    }
                    routeList.Add(routeTier.Distinct().ToList());
                }
                node.RouteData = routeList;
            }
        }

        public DateTime SimulationDate
        {
            get { return simulationDate; }
        }

        public Node GetNode(int id)
        {
            return Nodes[id];
        }

        public Path GetPath(int sender, int destination)
        {
            return Paths[Path.MakeCompositeID(sender, destination)];
        }

        public Path GetPathRandom(int num)
        {
            int i = 0;
            foreach (Path path in Paths.Values)
            {
                if (i==num) { return path; }
                i++;
            }
            return null;
        }

        public void ResetSimulationData()
        {
            foreach (Node node in Nodes.Values)
            {
                node.Reset();
            }
            foreach (Path path in Paths.Values)
            {
                path.Reset();
            }
            foreach (Link link in Links.Values)
            {
                link.Reset();
            }
        }

        public void CreateRoutingTable(DBManager dbMgr)
        {
            // リンク有無情報からルーティングテーブル作成.
            Dictionary<int, AdjacentNode> adjacentNodeDict = AdjacentNode.Fetch(dbMgr);
            Dictionary<int, RoutingTable> routingTableDict = RoutingTable.Fetch(dbMgr);
            foreach (RoutingTable routingTable in routingTableDict.Values)
            {
                routingTable.PrimaryKey += routingTableDict.Count;
                routingTable.Save();
            }
            for (int node1 = 1; node1<=NODE_NUM; node1++)
            {
                // node1のRoutingTableを作成する.
                Console.Write(String.Format("ノード{0}のルーティング情報を作成します。\n", node1));
                RoutingTable routingTable = RoutingTable.Get(dbMgr, node1);
                for (int node2 = 1; node2<=NODE_NUM; node2++)
                {
                    // node1からnode2へのホップ数を算出する.
                    if (node1==node2)
                    {
                        routingTable.SetHopCount(node1, 0);
                        continue;
                    }
                    List<AdjacentNode> currentNodeLintExt = new List<AdjacentNode> { adjacentNodeDict[node1] };
                    List<int> passedPath = new List<int> { node1 };
                    bool isComplete = false;
                    int hopCount = 1;

                    // 並列にルート算出を行いながらホップ数をカウントする.
                    while (!isComplete)
                    {
                        List<AdjacentNode> tmpLintExt = new List<AdjacentNode>();
                        foreach (AdjacentNode adjacentNode in currentNodeLintExt)
                        {
                            if (adjacentNode.AdjacentNodeList.Contains(node2))
                            {
                                routingTable.SetHopCount(node2, hopCount);
                                isComplete = true;
                                break;
                            }
                            foreach (int nodeId in adjacentNode.AdjacentNodeList)
                            {
                                if (!passedPath.Contains(nodeId))
                                {
                                    tmpLintExt.Add(adjacentNodeDict[nodeId]);
                                    passedPath.Add(nodeId);
                                }
                            }
                        }
                        currentNodeLintExt = tmpLintExt;
                        hopCount++;
                    }
                }
                Console.Write(String.Format("ノード{0}のルーティング情報を記録します。\n", node1));
                routingTable.Save();
            }
            Console.Write("ルーティング情報の作成が完了しました。\n");
        }

        public void CreatePathBandwidthTable(DBManager dbMgr)
        {
            // 光パスの占有波長域情報の作成.
            // TODO: 要新しいデータ形式への対応. 
            // TODO: 標準正規分布で占有波長域の割り当てを行いたい.
            Dictionary<int, Dictionary<int, int>> pathBandwidth = new Dictionary<int, Dictionary<int, int>> { };
            int count = 0;
            List<int> variation = new List<int> { 1, 2, 5, 10 };
            for (int node1 = 1; node1<=NODE_NUM; node1++)
            {
                Dictionary<int, int> nodeRoutingTable = new Dictionary<int, int> { };
                for (int node2 = 1; node2<=NODE_NUM; node2++)
                {
                    if (node2<=node1)
                    {
                        nodeRoutingTable[node2] = 0;
                    }
                    else
                    {
                        nodeRoutingTable[node2] = variation[count%variation.Count];
                        count++;
                    }
                }
                pathBandwidth[node1] = nodeRoutingTable;
            }

            // ルーティング情報書き込み.
            Console.Write("光パスの占有波長域情報を記録します。\n");
            Dictionary<string, object> where = new Dictionary<string, object>()
            {
                { "id", 0 }
            };
            for (int node1 = 1; node1<=NODE_NUM; node1++)
            {
                Dictionary<string, object> setData = new Dictionary<string, object> { };
                for (int node2 = 1; node2<=NODE_NUM; node2++)
                {
                    if (node2<=node1)
                    {
                        setData[node2.ToString()] = "0";
                    }
                    else
                    {
                        setData[node2.ToString()] = pathBandwidth[node1][node2].ToString();
                    }
                }
                where["id"] = node1;
                dbMgr.Update(PathBandwidth.GetTableName(), setData, where);
            }
            Console.Write("光パスの占有波長域情報の記録記録が完了しました。\n");
            Console.ReadLine();
        }
    }
}
