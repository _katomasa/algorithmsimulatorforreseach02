﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    public class AlgorithmConditionData
    {
        public int GroupId;
        public int AlgorithmId; // システムで追加するものは0, 設定するものは1以降の番号を振る.

        public Dictionary<int, AlgorithmCondition> AlgorithmList = new Dictionary<int, AlgorithmCondition>();

        public AlgorithmConditionData(int groupId, int algorithmId)
        {
            GroupId = groupId;
            AlgorithmId = algorithmId;
        }

        public int this[int x, int y]
        {
            get
            {
                return AlgorithmList[x].Conditions[y];
            }
        }

        public static Dictionary<int, List<AlgorithmConditionData>> RefactorAlgorithmList(List<AlgorithmCondition> algoList)
        {
            Dictionary<int, List<AlgorithmConditionData>> refactoredDict = new Dictionary<int, List<AlgorithmConditionData>>();
            Dictionary<int, Dictionary<int, AlgorithmConditionData>> algoDataDict = new Dictionary<int, Dictionary<int, AlgorithmConditionData>>();
            foreach (AlgorithmCondition algo in algoList)
            {
                if (!algoDataDict.ContainsKey(algo.GroupId))
                {
                    algoDataDict[algo.GroupId] = new Dictionary<int, AlgorithmConditionData>();
                }
                if (!algoDataDict[algo.GroupId].ContainsKey(algo.AlgorithmId))
                {
                    AlgorithmConditionData algoData = new AlgorithmConditionData(algo.GroupId, algo.AlgorithmId);
                    algoDataDict[algo.GroupId][algo.AlgorithmId] = algoData;
                }
                algoDataDict[algo.GroupId][algo.AlgorithmId].AlgorithmList[algo.Bandwidth] = algo;
            }
            foreach(int groupId in algoDataDict.Keys)
            {
                refactoredDict[groupId] = algoDataDict[groupId].Values.ToList();
            }
            return refactoredDict;
        }
    }
}
