﻿using MyDBLibrary;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    public class Simulation
    {
        /*
        シミュレーションを行うクラス.
        */
        public const int SIMULATION_COUNT = 100;

        private SimulationData sim;

        private int failedNum = 0;
        private DateTime startTime;
        private DateTime endTime;

        char[] bars = { '／', '―', '＼', '｜' };
        int barCounter = 0;
        private Random rand = new Random();

        public Simulation(SimulationData sim)
        {
            this.sim = sim;
        }

        public void Start(DBManager dbMgr, int groupId, AlgorithmConditionData algoData)
        {
            // simulate routing and spectrum allocation algorithms.
            // 1 timer Ticks = 100 nano seconds.
            int placedNum = 0;

            List<Path> pathList = new List<Path>();
            startTime = DateTime.Now;
            pathList = sortPath(algoData);
            foreach(Path path in pathList)
            {
                Console.Write("\r{0} 配置完了数: {1}/{2}", bars[barCounter%bars.Count()], placedNum, sim.Paths.Count());
                barCounter++;
                placedNum++;
                List<Route> routeList = getRouteData(algoData, path);
                Route proposalRoute = selectRoute(algoData, path, routeList);
                if (proposalRoute == null)
                {
                    failedNum++;
                    path.IsFailed = true;
                    continue;
                }
                selectPosition(algoData, path, proposalRoute);
                proposalRoute.SetPath(path, proposalRoute);
                UpdateBarCounter();
            }
            UpdateBarCounter();
            endTime = DateTime.Now;
            // シミュレーション結果の記録.
            saveResult(dbMgr, algoData);
            // シミュレーションデータのリセット.
            sim.ResetSimulationData();
            Console.Write("\r{0} 配置完了数: {1}/{2}", bars[barCounter%bars.Count()], placedNum, sim.Paths.Count());
            failedNum = 0;
            placedNum = 0;
        }

        public void End()
        {
            // OutputAllStates();
        }

        public void UpdateBarCounter()
        {
            Console.Write(String.Format("\r{0}", bars[barCounter%bars.Count()]));
            barCounter++;
        }

        private List<T> suffle<T>(List<T> list)
        {
            return new List<T>(list.OrderBy(x => Guid.NewGuid()));
        }

        private List<Path> sortPath(AlgorithmConditionData algoData)
        {
            // 光パスを優先順位に則ってソート.
            List<Path> pathList = new List<Path>();
            if (algoData.GroupId == (int)AlgorithmCondition.AlgorithmGroup.PathPriority)
            {
                Dictionary<int, List<Path>> tmpPathDict = new Dictionary<int, List<Path>>();
                foreach (Path path in sim.Paths.Values)
                {
                    UpdateBarCounter();
                    int priority = algoData[path.BandWidth, sim.RoutingTableDict[path.Node1].GetHopCount(path.Node2)];
                    if (!tmpPathDict.ContainsKey(priority)) { tmpPathDict[priority] = new List<Path>(); }
                    tmpPathDict[priority].Add(path);
                }
                Dictionary<int, List<Path>> pathDict = new Dictionary<int, List<Path>>();
                foreach (int priority in tmpPathDict.Keys)
                {
                    pathDict[priority] = suffle(tmpPathDict[priority]);
                }
                var sortedDict = pathDict.OrderBy((x) => x.Key);
                foreach (var v in sortedDict)
                {
                    UpdateBarCounter();
                    pathList.AddRange(v.Value);
                }
            }
            else
            {
                int pathNum = sim.Paths.Count;
                while (pathList.Count<pathNum)
                {
                    UpdateBarCounter();
                    var path = sim.GetPathRandom(rand.Next(pathNum));
                    if (!pathList.Contains(path))
                    {
                        pathList.Add(path);
                    }
                }
            }
            return pathList;
        }

        private int getTargetHopCount(AlgorithmConditionData algoData, Path path)
        {
            // 目標迂回距離情報を生成.
            int hopCount = sim.RoutingTableDict[path.Node1].HopCountList[path.Node2];
            if (algoData.GroupId == (int)AlgorithmCondition.AlgorithmGroup.AdditionalHopCount)
            {
                hopCount += algoData[path.BandWidth, hopCount];
            }
            else if (algoData.GroupId == (int)AlgorithmCondition.AlgorithmGroup.Random)
            {
                hopCount +=  rand.Next(AlgorithmCondition.MAX_ADDITIONAL_HOP_COUNT + 1);
            }
            return hopCount;
        }

        private List<Route> getRouteData(AlgorithmConditionData algoData, Path path)
        {
            // 配置可能な経路情報を取得.
            var targetHopCount = getTargetHopCount(algoData, path);
            List<List<int>> routeData = generateRouteNodeLists(path, targetHopCount);
            List<Route> routeList = generateRoutes(path, routeData);
            return getAvailableRoutes(algoData, path, routeList, targetHopCount);
        }

        private List<List<int>> generateRouteNodeLists(Path path, int targetHopCount)
        {
            // 各ホップ数で通る可能性があるノード情報から各ルートの経由ノード情報を生成.
            List<List<AdjacentNode>> targetRouteData = sim.Nodes[path.Node1].RouteData.GetRange(1, targetHopCount+SimulationData.ALLOW_HOP_MARGIN);
            List<List<int>> routeData = new List<List<int>> { new List<int> { path.Node1 } };
            foreach (List<AdjacentNode> tier in targetRouteData)
            {
                UpdateBarCounter();
                List<List<int>> tierRouteData = new List<List<int>>();
                foreach (List<int> route in routeData)
                {
                    UpdateBarCounter();
                    if (route.Last() == path.Node2)
                    {
                        tierRouteData.Add(route);
                        continue;
                    }
                    foreach (AdjacentNode adjacentNode in tier)
                    {
                        UpdateBarCounter();
                        if (adjacentNode.AdjacentNodeList.Contains(route.Last()))
                        {
                            if (!route.Contains(adjacentNode.NodeId))
                            {
                                List<int> newRoute = new List<int>();
                                newRoute.AddRange(route);
                                newRoute.Add(adjacentNode.NodeId);
                                tierRouteData.Add(newRoute);
                            }
                        }
                    }
                }
                routeData = tierRouteData;
            }
            return routeData;
        }

        private List<Route> generateRoutes(Path path, List<List<int>> routeData)
        {
            // 経由ノード情報から経由リンク情報をまとめたroute情報を作成.
            List<Route> routeList = new List<Route>();
            foreach (List<int> throughNodeList in routeData)
            {
                UpdateBarCounter();
                if (throughNodeList.Last()!=path.Node2) { continue; }
                // foreach内で使用する変数を初期化.
                List<Link> throughLinkList = new List<Link>();
                List<int> positionList = new List<int>();

                for (int i = 0; i<throughNodeList.Count-1; i++)
                {
                    UpdateBarCounter();
                    var link = sim.Links[Link.MakeCompositeID(throughNodeList[i], throughNodeList[i+1])];
                    throughLinkList.Add(link);
                    // 隣接しているリンクの空いているチャネル情報を保持.
                    if (i==0) { positionList = link.GetAvalablePositionList(path); }
                }
                routeList.Add(new Route(positionList, throughLinkList));
            }
            return routeList;
        }

        private List<Route> getAvailableRoutes(AlgorithmConditionData algoData, Path path, List<Route> routeList, int targetHopCount)
        {
            // 作成したRouteから配置可能なもののみ抽出.
            bool isAdditionalOrRandom = algoData.GroupId == (int)AlgorithmCondition.AlgorithmGroup.AdditionalHopCount || algoData.GroupId == (int)AlgorithmCondition.AlgorithmGroup.Random;
            // routeListから直接削除するので念のためイテレータ用にコピー.
            List<Route> copyRouteList = new List<Route>();
            copyRouteList.AddRange(routeList);
            foreach (Route route in copyRouteList)
            {
                UpdateBarCounter();
                // routeが配置可能かを判定する.
                foreach (Link link in route.ThroughLink)
                {
                    UpdateBarCounter();
                    if (!link.CanSetPath(path, route.PositionList))
                    {
                        routeList.Remove(route);
                        break;
                    }
                }
                if(isAdditionalOrRandom)
                {
                    if(targetHopCount+SimulationData.ALLOW_HOP_MARGIN < route.ThroughLink.Count)
                    {
                        routeList.Remove(route);
                    }
                }
            }

            // 目標迂回距離かランダムなら一番評価値に近い候補経路のみにする.
            if (isAdditionalOrRandom && 0 < routeList.Count)
            {
                routeList.Sort((a, b) => (int)(Math.Abs(targetHopCount - a.ThroughLink.Count) - Math.Abs(targetHopCount - b.ThroughLink.Count)));
                copyRouteList = new List<Route>();
                copyRouteList.AddRange(routeList);
                var bestHopCount = Math.Abs(targetHopCount - copyRouteList[0].ThroughLink.Count);
                foreach (Route route in copyRouteList)
                {
                    if(bestHopCount != Math.Abs(targetHopCount - route.ThroughLink.Count))
                    {
                        routeList.Remove(route);
                    }
                }
            }
            return routeList;
        }

        // 配置可能で使用率が高い方から割り当てる.
        private Route selectRoute(AlgorithmConditionData algoData, Path path, List<Route> routeList)
        {

            if(routeList.Count == 0) { return null; }

            // 選択するリンクの基準を設定.
            int choice = rand.Next((int)AlgorithmCondition.ChoiceLinkAlgorythm.Largest + 1);
            if (algoData.GroupId == (int)AlgorithmCondition.AlgorithmGroup.LinkUsage)
            {
                choice = algoData[path.BandWidth, sim.RoutingTableDict[path.Node1].HopCountList[path.Node2]];
            }

            // 基準に則ってソートし、最も適したルート情報を抽出.
            if (choice == (int)AlgorithmCondition.ChoiceLinkAlgorythm.Largest)
            {
                routeList.Sort((a, b) => (int)(a.LinkUsage - b.LinkUsage));
                return routeList[0];
            }
            else if (choice == (int)AlgorithmCondition.ChoiceLinkAlgorythm.Smallest)
            {
                routeList.Sort((a, b) => (int)(b.LinkUsage - a.LinkUsage));
                return routeList[0];
            }
            else
            {
                routeList.Sort((a, b) => (int)(a.LinkUsage - b.LinkUsage));
                return routeList[(routeList.Count/2)];
            }
        }

        private void selectPosition(AlgorithmConditionData algoData, Path path, Route proposalRoute)
        {
            // PositionList[0]に配置するため、目標波長配置に合わせてPositionListをソート.
            int suggestPosition = rand.Next(1, sim.LINK_CAPACITY+1);
            if (algoData.GroupId == (int)AlgorithmCondition.AlgorithmGroup.PlacedPosition)
            {
                suggestPosition = algoData[path.BandWidth, sim.RoutingTableDict[path.Node1].HopCountList[path.Node2]];
            }
            proposalRoute.PositionList.Sort((a, b) => (int)(Math.Abs(suggestPosition - a) - Math.Abs(suggestPosition - b)));
            UpdateBarCounter();
        }

        private void saveResult(DBManager dbMgr, AlgorithmConditionData algoData)
        {
            // 結果を記録.
            AlgorithmCondition algo = algoData.AlgorithmList[(int)PathBandwidth.Bandwidth.A];
            SimulationResult result = new SimulationResult(dbMgr);
            result.GroupId = algo.GroupId;
            result.GroupName = algo.GroupName;
            result.AlgorithmId = algo.AlgorithmId;
            result.AlgorithmName = algo.AlgorithmName;
            result.NetworkUsage = CalculateNetworkUsage();
            result.UnusedNetworkResource = 1.0-result.NetworkUsage;
            result.PathBandwidthUsage = CalculatePathBandwidthUsage();
            result.ProcessingTime = (int)(endTime.Ticks - startTime.Ticks);
            result.FailedNum = failedNum;
            result.SimulationDate = sim.SimulationDate;
            result.Insert();
        }

        public void SimulationEnd(DBManager db_mgr)
        {
            // evaluate algorithm.
            OutputAllStates();
            return;
        }

        private double CalculateNetworkUsage()
        {
            double totalUsage = 0;
            foreach (Link link in sim.Links.Values)
            {
                totalUsage += link.Usage;
            }
            return totalUsage / (sim.Links.Count * sim.LINK_CAPACITY);
        }

        private double CalculatePathBandwidthUsage()
        {
            double totalBandwidth = 0;
            double totalSetBandwidth = 0;
            foreach (Path path in sim.Paths.Values)
            {
                totalBandwidth += path.BandWidth;
                if (!path.IsFailed) { totalSetBandwidth += path.BandWidth; }
            }

            return totalSetBandwidth / totalBandwidth;
        }

        private void OutputAllStates()
        {
            // TODO: 新しいデータ形式でテキストまたはCSVファイル形式で出力できるように修正する.
            if (!Directory.Exists(String.Format(@"..\..\..\results\JPN{0}", sim.NODE_NUM)))
                Directory.CreateDirectory(String.Format(@"..\..\..\results\JPN{0}", sim.NODE_NUM));

            for (int i=1; i<=sim.NODE_NUM; i++)
            {
                string nodeStateLog = string.Format(@"..\..\..\results\JPN{0}\node_{1}.txt", sim.NODE_NUM, i);
                string contents = sim.GetNode(i).Result();
                File.WriteAllText(nodeStateLog, contents);
            }

            string result = string.Format(@"..\..\..\results\JPN{0}\result_{1}.txt", sim.NODE_NUM, endTime.ToString("yyyyMMdd-HHmmss-ffff"));
            string log = "";
            foreach (Path path in sim.Paths.Values){
                log += String.Format("Path {0}: {1}\t{2}\n", path.Id.ToString(), path.BandWidth, path.CheckIsFailedByString());
            }
            log += "\n";
            //foreach (Link link in sim.Links.Values)
            //{
            //    for (int i=1; i<=link.LinkNum; i++)
            //    {
            //        log += String.Format("Link {0}: {1}\t{2}/{3}\t(rest: {4})\n", link.ID.ToString(), i.ToString(), 
            //            link.GetUsage(i).ToString(), sim.LINK_CAPACITY.ToString(), link.GetRest(i).ToString());
            //    }
            //}
            log += "\n\n";
            File.WriteAllText(result, log);
        }
    }
}
