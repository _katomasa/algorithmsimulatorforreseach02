﻿using MyDBLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    public class SimulationProgram
    {
        public static void Main(String[] args)
        {
            // JPN12とJPN25のどちらのシミュレーションか.
            Console.Write("Do you want to excute JPN12 simulation? (y/n): ");
            bool isJPN12 = YesOrNo();

            // DB環境の設定.
            string dbName;
            string assemblyname = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            string path = "";
            using (var stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(assemblyname + ".solutionpath.txt"))
            {
                using (var sr = new StreamReader(stream))
                {
                    path = sr.ReadToEnd().Trim();
                }
            }
            if (isJPN12) { dbName = path + @"Conditions\JPN12\jpn12.db"; }
            else { dbName = path + @"Conditions\JPN25\jpn25.db"; }
            DBManager dbMgr = new DBManager(dbName);

            bool is_empty_condition = false;
            bool is_empty_node = false;
            bool is_empty_path = false;

            // 存在していないテーブルを作成.
            if (!dbMgr.CheckExistanceTable(AlgorithmCondition.GetTableName()))
            {
                dbMgr.CreateTable(new AlgorithmCondition());
                is_empty_condition = true;
            }
            if (!dbMgr.CheckExistanceTable(SimulationResult.GetTableName()))
            {
                dbMgr.CreateTable(new SimulationResult());
            }
            if (!dbMgr.CheckExistanceTable(AdjacentNode.GetTableName()))
            {
                dbMgr.CreateTable(new AdjacentNode());
                is_empty_node = true;
            }
            if (!dbMgr.CheckExistanceTable(PathBandwidth.GetTableName()))
            {
                dbMgr.CreateTable(new PathBandwidth());
                is_empty_path = true;
            }
            if (!dbMgr.CheckExistanceTable(RoutingTable.GetTableName()))
            {
                dbMgr.CreateTable(new RoutingTable());
            }

            // 光パス配置条件、隣接ノード情報、光パス条件がない場合はいったん終了.
            if(is_empty_condition || is_empty_node || is_empty_path)
            {
                Console.WriteLine("Conditions or/and Adjacent Node or/and Path Bandwidth are empty.\nPlease register thier data to db.");
                Console.ReadLine();
                return;
            }

            SimulationData simData = new SimulationData(dbMgr, isJPN12);

            Console.Write("Do you want to create settings automatically? (y/n): ");
            bool option = YesOrNo();
            if (option)
            {
                // 隣接ノード情報からルーティングテーブルを作成する.
                Console.Write("Do you want to create routing table by link existance? (y/n): ");
                bool create = YesOrNo();
                if (create)
                {
                    simData.CreateRoutingTable(dbMgr);
                }
                // 正規分布によって各占有波長域の光パスの本数を求め、光パスに割り当てる.
                //Console.Write("Do you want to create path bandwidth setting by average? (y/n): ");
                //create = YesOrNo();
                //if (create)
                //{
                //    simData.CreatePathBandwidthTable(dbMgr);
                //}
            }

            // シミュレーションを実行.
            Simulation simulation = new Simulation(simData);
            Console.WriteLine(String.Format("\rSimulation start!"));
            foreach (AlgorithmCondition.AlgorithmGroup groupId in Enum.GetValues(typeof(AlgorithmCondition.AlgorithmGroup)))
            {
                if (groupId == AlgorithmCondition.AlgorithmGroup.Random)
                {
                    // パス優先度を定めるAlgorithmGroupであれば、
                    // ランダムに配置するパスを選択するアルゴリズム用にID:0のAlgorithmConditionDataを追加.
                    // 指定してないアルゴリズムは全てランダムになっているので実質全てランダムになっているアルゴリズム.
                    AlgorithmConditionData randomSortAlgoData = new AlgorithmConditionData((int)groupId, 0);
                    randomSortAlgoData.AlgorithmList = new Dictionary<int, AlgorithmCondition> { { (int)PathBandwidth.Bandwidth.A, new AlgorithmCondition() } };
                    randomSortAlgoData.AlgorithmList[(int)PathBandwidth.Bandwidth.A].AlgorithmName = "Random Algorithm";
                    randomSortAlgoData.AlgorithmList[(int)PathBandwidth.Bandwidth.A].GroupName = "random";
                    simData.AlgorithmConditionDict[(int)groupId] = new List<AlgorithmConditionData>();
                    simData.AlgorithmConditionDict[(int)groupId].Add(randomSortAlgoData);
                }

                // groupIdのグループが存在していたら全部SIMULATION_COUNT回シミュレーションを行う.
                if (simData.AlgorithmConditionDict.ContainsKey((int)groupId))
                {
                    foreach (AlgorithmConditionData algoData in simData.AlgorithmConditionDict[(int)groupId])
                    {
                        var algorithmId = algoData.AlgorithmList[(int)PathBandwidth.Bandwidth.A].AlgorithmId;
                        var algorithmName = algoData.AlgorithmList[(int)PathBandwidth.Bandwidth.A].AlgorithmName;
                        Console.WriteLine(String.Format("\nAlgorithm Group: ID: {0} (Name: {1})", groupId, algorithmName));
                        simulation.UpdateBarCounter();
                        for (int i = 1; i<=Simulation.SIMULATION_COUNT; i++)
                        {
                            Console.Write(String.Format("\r― 配置完了数: 0/{0}   Trial: {1}/{2} (ID: {3}  Name: {4})  ", simData.Paths.Count(), i, Simulation.SIMULATION_COUNT, algorithmId, algorithmName));
                            simulation.UpdateBarCounter();
                            simulation.Start(dbMgr, (int)groupId, algoData);
                            simulation.End();
                        }
                        Console.Write("\r配置完了数: {0}/{1}    ", simData.Paths.Count(), simData.Paths.Count());
                    }
                }
            }
            Console.WriteLine(String.Format("\nSimulation end!"));

            Console.ReadLine();
        }

        public static bool YesOrNo()
        {
            var input = Console.ReadLine();
            bool result;
            while (true)
            {
                if (input == "y" || input == "Y")
                {
                    result = true;
                    break;
                }
                else if (input == "n" || input == "N")
                {
                    result = false;
                    break;
                }
                else
                {
                    Console.Write("please input y/n. :");
                    input = Console.ReadLine();
                }
            }
            return result;
        }
    }
}
