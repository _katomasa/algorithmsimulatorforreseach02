﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDBLibrary
{
    public interface IField
    {
        string GetFieldType();
        string GetFieldName();
        void ConvertToValue(SQLiteDataReader reader);
        string ConvertToSQL();
    }

    // DBModelに使用するFieldのクラス. これを変数の代わりに使用する.
    public abstract class BaseField<T>: IField
    {
        protected string type;
        protected string name;
        protected string value;

        // Valueプロパティは各継承クラスで定義する.
        public abstract T Value { set; get; }

        // DBでのフィールドの型を取得する.
        public string GetFieldType()
        {
            return type;
        }

        // フィールド名を取得する.
        public string GetFieldName()
        {
            return name;
        }

        /* 
        DBから取得したデータから値を取得. 
        現在は内部的にはStringで持っているので共通だが、変更する場合はメソッドをオーバライドする必要がある.
        */
        public void ConvertToValue(SQLiteDataReader reader)
        {
            value = reader[name].ToString();
        }

        // 値をDBに記録するために文字列に変換.
        public string ConvertToSQL()
        {
            return value;
        }
    }

    // 整数型のField.
    public class IntField : BaseField<int>
    {
        public IntField()
        {
            type = "integer";
        }

        public IntField(string fieldName, int defaultValue = 0)
        {
            type = "integer";
            name = fieldName;
            value = defaultValue.ToString();
        }

        public override int Value
        {
            set { this.value = value.ToString(); }
            get { return int.Parse(this.value); }
        }
    }

    // 実数型のField.
    public class RealField : BaseField<double>
    {
        public RealField()
        {
            type = "real";
        }

        public RealField(string fieldName, int defaultValue = 0)
        {
            type = "real";
            name = fieldName;
            value = defaultValue.ToString();
        }

        public override double Value
        {
            set { this.value = value.ToString(); }
            get { return double.Parse(this.value); }
        }
    }

    // テキスト型のField.
    public class TextField : BaseField<string>
    {
        public TextField()
        {
            type = "text";
        }

        public TextField(string fieldName, string defaultValue = "")
        {
            type = "text";
            name = fieldName;
            value = defaultValue;
        }

        public override string Value
        {
            set { this.value = value; }
            get { return value; }
        }
    }

    // List型のField. DB上ではTextで記録する.
    public class ListField : BaseField<List<string>>
    {
        public ListField()
        {
            type = "text";
        }

        public ListField(string fieldName, string defaultValue = "")
        {
            type = "text";
            name = fieldName;
            value = defaultValue;
        }

        public override List<string> Value
        {
            set
            {
                this.value = String.Join(",", value);
            }
            get
            {
                char[] delimiterChars = { ' ', ',' };
                List<string> list = new List<string>();
                if(value != "")
                {
                    list.AddRange(value.Split(delimiterChars));
                }
                return list;
            }
        }

        public string this[int x]
        {
            set
            {
                char[] delimiterChars = { ' ', ',' };
                List<string> list = new List<string>();
                if (value != "")
                {
                    list.AddRange(this.value.Split(delimiterChars));
                }
                list[x] = value;
                this.value = String.Join(",", list);
            }
            get
            {
                char[] delimiterChars = { ' ', ',' };
                List<string> list = new List<string>();
                if (value != "")
                {
                    list.AddRange(value.Split(delimiterChars));
                }
                return list[x];
            }
        }

        public void Add(int i)
        {
            if(value == "")
            {
                value += i.ToString();
            }
            else
            {
                value += "," + i.ToString();
            }
        }

        public static void SetIntList(ListField field, List<int> value)
        {
            List<string> list = new List<string>();
            foreach (int nodeId in value)
            {
                list.Add(nodeId.ToString());
            }
            field.Value = list;
        }

        public static List<int> GetIntList(ListField field)
        {
            List<int> list = new List<int>();
            foreach (string nodeId in field.Value)
            {
                var i = int.Parse(nodeId);
                list.Add(i);
            }
            return list;
        }
    }

    // DateTime型のField. DB上ではTextで記録する.
    public class DateTimeField : BaseField<DateTime>
    {
        public DateTimeField()
        {
            type = "text";
        }

        public DateTimeField(string fieldName, string defaultValue = "")
        {
            type = "text";
            name = fieldName;
            value = defaultValue;
        }

        public override DateTime Value
        {
            set { this.value = value.ToString("yyyy/MM/dd HH:mm:ss"); }
            get { return DateTime.ParseExact(value, "yyyy/MM/dd HH:mm:ss", null); }
        }
    }

    // DateTime型のField. DB上では日付のみのTextで記録する.
    public class DateField : BaseField<DateTime>
    {
        public DateField()
        {
            type = "text";
        }

        public DateField(string fieldName, string defaultValue = "")
        {
            type = "text";
            name = fieldName;
            value = defaultValue;
        }

        public override DateTime Value
        {
            set { this.value = value.ToString("yyyyMMdd"); }
            get { return DateTime.ParseExact(value, "yyyyMMdd", null); }
        }
    }
}
