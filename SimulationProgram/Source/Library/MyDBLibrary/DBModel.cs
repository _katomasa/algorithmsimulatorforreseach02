﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyDBLibrary
{
    public abstract class BaseDBModel
    {
        // インスタンスメソッドからのDBへのアクセス用.
        protected DBManager dbMgr;
        // 式木で作成したインスタンス生成メソッドのキャッシュ.
        protected static Dictionary<Type, Func<DBManager, BaseDBModel>> cache = new Dictionary<Type, Func<DBManager, BaseDBModel>>();
        protected static string tableName = "";    // staticにしたいが、現在のしようじゃ無理なので通常の変数. コンパイル時にJPN12とJPN25で切り替えられるようにすれば行けるか？
        protected static string primaryKeyName = "id";
        protected List<IField> fields = new List<IField>();   // Field一覧.

        // 主キーのフィールド.
        protected IntField primaryKey = new IntField(primaryKeyName);

        // コンストラクタ.
        public BaseDBModel(DBManager dbMgr=null)
        {
            if (dbMgr==null) { dbMgr = new DBManager(@"simulation.db"); }
            this.dbMgr = dbMgr;
            fields.Add(primaryKey);
        }

        // プロパティ.
        public int PrimaryKey
        {
            set { primaryKey.Value = value; }
            get { return primaryKey.Value; }
        }

        // テーブル名プロパティ.
        public abstract string TableName { get; }

        // メソッド.
        // 静的な主キー名を取得. 
        public static string GetPrimaryKeyName()
        {
            return primaryKeyName;
        }

        // Fieldのリストを取得.
        public List<IField> GetFields()
        {
            return fields;
        }

        // インスタンス生成メソッドを呼び出し実行. 存在しない場合はコンパイルしてキャッシュする.
        private static T create<T>(DBManager dbMgr)
            where T : BaseDBModel
        {
            Type t = typeof(T);
            Func<DBManager, BaseDBModel> d;
            if (!cache.TryGetValue(t, out d))
            {
                Console.Write(String.Format("Compile create method: {0}\n", t.ToString()));
                var argsTypes = new[] { typeof(DBManager) };
                var constructor = typeof(T).GetConstructor(BindingFlags.Public | BindingFlags.Instance, Type.DefaultBinder, argsTypes, null);
                var args = argsTypes.Select(Expression.Parameter).ToArray();
                d = Expression.Lambda<Func<DBManager, BaseDBModel>>(Expression.New(constructor, args), args).Compile();
                cache[t] = d;
            }
            return (T)d(dbMgr);
        }

        // DBに記録する. まずInsertで試して、だめだったらUpdateで記録. 2クエリ発行するのでいずれ1クエリで済むようにしたい.
        public void Save()
        {
            try
            {
                Insert();
            }
            catch(SQLiteException err)
            {
                if(err.ResultCode == SQLiteErrorCode.Constraint)
                {
                    Update();
                }
                else
                {
                    throw err;
                }
            }
        }

        // インスタンスのデータをInsert.
        public void Insert()
        {
            Dictionary<string, object> data = new Dictionary<string, object> { };
            for (int i=0; i<fields.Count; i++)
            {
                data[fields[i].GetFieldName()] = fields[i].ConvertToSQL();
            }
            dbMgr.Insert(TableName, data);
        }

        // インスタンスのデータでUpdate.
        public void Update()
        {
            Dictionary<string, object> data = new Dictionary<string, object> { };
            Dictionary<string, object> where = new Dictionary<string, object> {{GetPrimaryKeyName(), PrimaryKey}};
            for (int i=0; i<fields.Count; i++)
            {
                data[fields[i].GetFieldName()] = fields[i].ConvertToSQL();
            }
            dbMgr.Update(TableName, data, where);
        }

        // インスタンスのIDのレコードをDelete.
        public void Delete()
        {
            dbMgr.Delete(TableName, PrimaryKey);
        }

        public string CreateTableQuery()
        {
            return "";
        }

        // 指定した主キーのデータを取得する. ジェネリックなので呼び出すときは取得したいBaseDBModelの継承クラスを指定.
        public static T GetById<T>(DBManager dbMgr, int id)
            where T : BaseDBModel
        {
            var ins = create<T>(dbMgr);
            using (SQLiteConnection con = dbMgr.Connect())
            {
                using (SQLiteDataReader reader = dbMgr.Select(con, ins.TableName, where: new Dictionary<string, object> { { BaseDBModel.GetPrimaryKeyName(), id } }))
                {
                    if (reader.Read())
                    {
                        foreach (IField field in ins.GetFields())
                        {
                            field.ConvertToValue(reader);
                        }
                    }
                    else
                    {
                        ins.PrimaryKey = id;
                    }
                    return (T)ins;
                }
            }
        }

        // 指定した条件のデータを取得.
        public static T Get<T>(DBManager dbMgr, Dictionary<string, object> where)
            where T : BaseDBModel
        {
            var ins = create<T>(dbMgr);
            using (SQLiteConnection con = dbMgr.Connect())
            {
                using (SQLiteDataReader reader = dbMgr.Select(con, ins.TableName, where: where))
                {
                    if (reader.Read())
                    {
                        foreach (IField field in ins.GetFields())
                        {
                            field.ConvertToValue(reader);
                        }
                    }
                    return (T)ins;
                }
            }
        }

        // データを一括で取得.
        public static List<T> Fetch<T>(DBManager dbMgr, Dictionary<string, object> where = null)
            where T : BaseDBModel
        {
            List<T> modelList = new List<T>();
            var ins = create<T>(dbMgr);
            using (SQLiteConnection con = dbMgr.Connect())
            {
                using (SQLiteDataReader reader = dbMgr.Select(con, ins.TableName, where: where))
                {
                    while (reader.Read())
                    {
                        var model = create<T>(dbMgr);
                        foreach (IField field in model.GetFields())
                        {
                            field.ConvertToValue(reader);
                        }
                        modelList.Add(model);
                    }
                    reader.Close();
                    return modelList;
                }
            }
        }
    }
}
