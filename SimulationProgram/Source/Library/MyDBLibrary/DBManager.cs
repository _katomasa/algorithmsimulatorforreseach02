﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyDBLibrary
{
    public class DBManager
    {
        private string connectionString;

        // コンストラクタ.
        public DBManager(string dbName=@"simulation.db")
        {
            connectionString = "Data Source=" + dbName;
        }

        /* Connectionを取得する.
        データを取得するときのみ、SQLiteDataReaderを扱うメソッドでConnectionを取得して引数で回す必要がある.*/
        public SQLiteConnection Connect()
        {
            return new SQLiteConnection(connectionString);
        }

        // データの書き込み.
        private void writeDB(string query)
        {
            using (SQLiteConnection con = new SQLiteConnection(connectionString))
            {
                con.Open();
                using (SQLiteTransaction tr = con.BeginTransaction())
                {
                    using (SQLiteCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                    tr.Commit();
                }
                con.Close();
            }
        }

        // データの読み込み.
        // この中でusingを使ってConnectionのリソース管理を行うと取得できないので、
        // 利用先で管理しインスタンスを生成してデータを返すようにする.
        private SQLiteDataReader readDB(SQLiteConnection con, string query)
        {
            con.Open();
            using (SQLiteCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = query;
                SQLiteDataReader reader = cmd.ExecuteReader();
                return reader;
            }
        }

        /* 
        これらの関数は条件と一致したものを操作するなど最低限のことしかできないので、
        GreaterThanとかで指定したり、Group Byなどを使用する場合は拡充が必要.
        */
        // Select.
        public SQLiteDataReader Select(SQLiteConnection con, string table, string[] columns = null, Dictionary<string, object> where = null)
        {
            string select = "select {0} from `{1}`{2};";
            string columnBase = "";

            // columnsが指定されていたら指定カラムのみ取ってくる.
            if (columns != null)
            {
                foreach (string column in columns)
                {
                    columnBase += "`" + column + "`, ";
                }
                columnBase = columnBase.Remove(columnBase.Length - 2);
            }
            else
            {
                columnBase += "*";
            }

            // whereが指定されていたらWHERE句を追加.
            // 後日、もっと自由に指定できるように変更.
            string whereStr = "";
            if (where != null)
            {
                whereStr = "WHERE ";
                foreach (string key in where.Keys)
                {
                    whereStr += String.Format("{0}={1}", key, where[key]);
                    break;
                }
            }
            select = String.Format(select, columnBase, table, whereStr);

            try
            {
                return readDB(con, select);
            }
            catch
            {
                return null;
            }
        }

        // Insert.
        public void Insert(string table, Dictionary<string, object> data)
        {

            string insert = "insert into `{0}` ({1}) values ({2});";
            string columns = "";
            string values = "";

            // INSERTするデータをQueryに変換する.
            foreach (string key in data.Keys)
            {
                columns += "`" + key + "`, ";
                values += "'" + data[key] + "', ";
            }
            columns = columns.Remove(columns.Length - 2);
            values = values.Remove(values.Length - 2);
            insert = String.Format(insert, table, columns, values);

            writeDB(insert);
        }

        // Update.
        public void Update(string table, Dictionary<string, object> setData, Dictionary<string, object> where)
        {
            string update = "UPDATE `{0}` SET {1}{2};";
            string set = "";

            // UPDATEするデータをクエリに変換する.
            foreach (string key in setData.Keys)
            {
                set += String.Format("`{0}`='{1}', ", key, setData[key]);
            }
            set = set.Remove(set.Length - 2, 1);

            // whereが指定されていたらWHERE句を追加. 指定がないと全レコード更新になる.
            string whereStr = "";
            if (0 < where.Count)
            {
                whereStr = "WHERE ";
                foreach (string key in where.Keys)
                {
                    whereStr += String.Format("{0}={1}", key, where[key]);
                    break;
                }
            }
            update = String.Format(update, table, set, whereStr);

            writeDB(update);
        }

        // Delete.
        public void Delete(string table, int id)
        {
            // 指定したIDのレコードを削除.
            writeDB(String.Format("DELETE FROM {0} WHERE `id`={1};", table, id.ToString()));
        }

        // BaseDBModelを参照してテーブルを作成.
        public void CreateTable(BaseDBModel model)
        {
            // テーブルが存在しているかをチェック
            string tableName = model.TableName;

            if (CheckExistanceTable(tableName)) { return; }

            // Queryを発行.
            string createTable = "CREATE TABLE {0} ({1} PRIMARY KEY{2});";
            string columns = "";
            foreach(IField field in model.GetFields())
            {
                columns += String.Format("{0} {1}, ", field.GetFieldName(), field.GetFieldType());
            }
            for (int i = 0; i<model.GetFields().Count; i++)
            {
            }

            // 複合キーの時の処理を考える.
            string compositeKey = "(id)";

            createTable = String.Format(createTable, tableName, columns, compositeKey);

            writeDB(createTable);
            Console.Write(String.Format("Created table: {0}.\n", tableName));
        }

        // テーブルが存在しているかチェック.
        public bool CheckExistanceTable(string tableName)
        {
            using (SQLiteConnection con = new SQLiteConnection(connectionString))
            {
                using (SQLiteDataReader reader = Select(con, tableName))
                {
                    if (reader != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}
