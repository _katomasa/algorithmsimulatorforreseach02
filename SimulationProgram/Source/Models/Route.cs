﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    public class Route
    {
        private List<int> positionList;
        private List<Link> throughLink;
        private double linkUsage = 0;

        public Route(List<int> positionList, List<Link> throughLink)
        {
            this.positionList = positionList;
            this.throughLink = throughLink;
            int totalCapacity = 0;
            int totalUsage = 0;
            foreach(Link link in throughLink)
            {
                totalCapacity += link.Capacity;
                totalUsage += link.Usage;
            }
            linkUsage = (double)totalUsage/(double)totalCapacity;
        }

        public List<int> PositionList
        {
            get { return positionList; }
        }

        public List<Link> ThroughLink
        {
            get { return throughLink; }
        }

        public int Position
        {
            get { return positionList[0]; }
        }

        public double LinkUsage
        {
            get { return linkUsage; }
        }

        public void SetPath(Path path, Route route)
        {
            foreach (Link link in route.ThroughLink)
            {
                link.SetPath(path, route.Position);
            }
        }
    }
}
