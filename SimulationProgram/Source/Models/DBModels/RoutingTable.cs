﻿using MyDBLibrary;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    // ルーティングテーブルモデル.
    public class RoutingTable : BaseDBModel
    {
        private new static string tableName = "routing_table";

        private IntField nodeId = new IntField("node_id");
        private ListField hopCountList = new ListField("hop_cnt");

        public RoutingTable(DBManager dbMgr = null) : base(dbMgr)
        {
            fields.Add(nodeId);
            fields.Add(hopCountList);
            List<int> init = new List<int>();
            for(int i=0; i < SimulationData.JPN25_NODE_NUM; i++)
            {
                init.Add(0);
            }
            HopCountList = init;
        }

        // テーブル名プロパティ.
        public override string TableName
        {
            get { return tableName; }
        }

        // 静的なテーブル名を取得. 
        public static string GetTableName()
        {
            return tableName;
        }

        public int NodeId
        {
            set { nodeId.Value = value; }
            get { return nodeId.Value; }
        }

        public List<int> HopCountList
        {
            set { ListField.SetIntList(hopCountList, value); }
            get { return ListField.GetIntList(hopCountList); }
        }

        public void SetHopCount(int nodeId, int hopCnt)
        {
            hopCountList[nodeId-1] = hopCnt.ToString();
        }

        public int GetHopCount(int nodeId)
        {
            return int.Parse(hopCountList[nodeId-1]);
        }

        public static RoutingTable Get(DBManager dbMgr, int nodeId)
        {
            RoutingTable ins = BaseDBModel.GetById<RoutingTable>(dbMgr, nodeId);
            ins.NodeId = nodeId;
            return ins;
        }

        public static Dictionary<int, RoutingTable> Fetch(DBManager dbMgr)
        {
            Dictionary<int, RoutingTable> routingTableDict = new Dictionary<int, RoutingTable>();
            List<RoutingTable> routingTableList = BaseDBModel.Fetch<RoutingTable>(dbMgr);
            foreach (RoutingTable model in routingTableList)
            {
                routingTableDict[model.NodeId] = model;
            }
            return routingTableDict;
        }
    }
}
