﻿using MyDBLibrary;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    // リンク有無データモデル.
    public class AdjacentNode : BaseDBModel
    {
        private new static string tableName = "adjacent_node";

        // DBで扱うフィールド.
        private IntField nodeId = new IntField("node_id");
        private ListField adjacentNodeList = new ListField("adjacent_node");

        // コンストラクタ.
        public AdjacentNode(DBManager dbMgr = null) : base(dbMgr)
        {
            fields.Add(nodeId);
            fields.Add(adjacentNodeList);
        }

        // プロパティ.
        public override string TableName
        {
            get { return tableName; }
        }

        public int NodeId
        {
            set { nodeId.Value = value; }
            get { return nodeId.Value; }
        }

        public List<int> AdjacentNodeList
        {
            set { ListField.SetIntList(adjacentNodeList, value); }
            get { return ListField.GetIntList(adjacentNodeList); }
        }

        // メソッド.
        // 静的なテーブル名を取得. 
        public static string GetTableName()
        {
            return tableName;
        }

        // 隣接ノード情報にノード追加.
        public void AddAdjacentNodeList(int adjacentNode)
        {
            adjacentNodeList.Add(adjacentNode);
        }

        // BaseDBModel.Get<T>メソッドを利用して、DBから指定したノードIDの隣接ノード情報を取得する.
        public static AdjacentNode Get(DBManager dbMgr, int nodeId)
        {
            AdjacentNode ins = BaseDBModel.GetById<AdjacentNode>(dbMgr, nodeId);
            ins.NodeId = nodeId;
            return ins;
        }

        // スタティックなデータなので一括で取得できるようにする.
        static public Dictionary<int, AdjacentNode> Fetch(DBManager dbMgr)
        {
            Dictionary<int, AdjacentNode> adjacentNodeDict = new Dictionary<int, AdjacentNode>();
            List<AdjacentNode> adjacentNodeList = BaseDBModel.Fetch<AdjacentNode>(dbMgr);
            foreach(AdjacentNode model in adjacentNodeList)
            {
                adjacentNodeDict[model.NodeId] = model;
            }
            return adjacentNodeDict;
        }
    }
}
