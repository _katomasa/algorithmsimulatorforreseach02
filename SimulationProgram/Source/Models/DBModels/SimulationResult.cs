﻿using MyDBLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    // シミュレーション結果モデル.
    public class SimulationResult : BaseDBModel
    {
        private new static string tableName = String.Format("simulation_result_{0}", DateTime.Now.ToString("yyyyMMdd"));

        private IntField groupId = new IntField("group_id");
        private TextField groupName = new TextField("group_name");
        private IntField algorithmId = new IntField("algorithm_id");
        private TextField algorithmName = new TextField("algorithm_name");
        private RealField networkUsage = new RealField("network_usage");
        private RealField unusedNetworkResource = new RealField("unused_network_resource");
        private RealField pathBandwidthUsage = new RealField("path_bandwidth_usage");
        private IntField processingTime = new IntField("processing_time");
        private IntField failedNum = new IntField("failed_cnt");
        private DateTimeField simulationDate = new DateTimeField("simulation_date");

        public SimulationResult(DBManager dbMgr=null) : base(dbMgr)
        {
            fields.Add(groupId);
            fields.Add(groupName);
            fields.Add(algorithmId);
            fields.Add(algorithmName);
            fields.Add(networkUsage);
            fields.Add(unusedNetworkResource);
            fields.Add(pathBandwidthUsage);
            fields.Add(processingTime);
            fields.Add(failedNum);
            fields.Add(simulationDate);
        }

        // プロパティ.
        public override string TableName
        {
            get { return tableName; }
        }

        public int GroupId
        {
            set { groupId.Value = value; }
            get { return groupId.Value; }
        }

        public string GroupName
        {
            set { groupName.Value = value; }
            get { return groupName.Value; }
        }

        public int AlgorithmId
        {
            set { algorithmId.Value = value; }
            get { return algorithmId.Value; }
        }

        public string AlgorithmName
        {
            set { algorithmName.Value = value; }
            get { return algorithmName.Value; }
        }

        public double NetworkUsage
        {
            set { networkUsage.Value = value; }
            get { return networkUsage.Value; }
        }

        public double UnusedNetworkResource
        {
            set { unusedNetworkResource.Value = value; }
            get { return unusedNetworkResource.Value; }
        }

        public double PathBandwidthUsage
        {
            set { pathBandwidthUsage.Value = value; }
            get { return pathBandwidthUsage.Value; }
        }

        public int ProcessingTime
        {
            set { processingTime.Value = value; }
            get { return processingTime.Value; }
        }

        public int FailedNum
        {
            set { failedNum.Value = value; }
            get { return failedNum.Value; }
        }

        public DateTime SimulationDate
        {
            set { simulationDate.Value = value; }
            get { return simulationDate.Value; }
        }

        // 静的なテーブル名を取得. 
        public static string GetTableName()
        {
            return tableName;
        }

        /* 主キーを自動で振りたいので、主キーフィールドを取り除いてからInsert.
        基本的にプログラムから取得しないが取得したい場合は主キー以外のカラムで取得するメソッドを作成する.*/
        public new void Insert()
        {
            fields.Remove(primaryKey);
            base.Insert();
        }
    }
}
