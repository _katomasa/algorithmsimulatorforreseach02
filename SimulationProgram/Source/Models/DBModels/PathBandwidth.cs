﻿using MyDBLibrary;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    // 光パス占有波長域モデル.
    public class PathBandwidth : BaseDBModel
    {
        public enum Bandwidth
        {
            A = 1,
            B = 2,
            C = 5,
            D = 10
        }

        private new static string tableName = String.Format("path_bandwidth_{0}", DateTime.Now.ToString("yyyyMMdd"));

        private IntField nodeId = new IntField("node_id");
        private ListField bandwidthIdList = new ListField("bandwidth");

        public PathBandwidth(DBManager dbMgr = null) : base(dbMgr)
        {
            fields.Add(nodeId);
            fields.Add(bandwidthIdList);
            List<int> init = new List<int>();
            for (int i = 0; i < SimulationData.JPN25_NODE_NUM; i++)
            {
                init.Add(0);
            }
            BandwidthIdList = init;
        }

        // テーブル名プロパティ.
        public override string TableName
        {
            get { return tableName; }
        }

        // 静的なテーブル名を取得. 
        public static string GetTableName()
        {
            return tableName;
        }

        public int NodeId
        {
            set { nodeId.Value = value; }
            get { return nodeId.Value; }
        }

        public List<int> BandwidthIdList
        {
            set { ListField.SetIntList(bandwidthIdList, value); }
            get { return ListField.GetIntList(bandwidthIdList); }
        }

        public void SetHopCount(int nodeId, int hopCnt)
        {
            bandwidthIdList[nodeId-1] = hopCnt.ToString();
        }

        public int GetHopCount(int nodeId)
        {
            return int.Parse(bandwidthIdList[nodeId-1]);
        }

        public static PathBandwidth Get(DBManager dbMgr, int nodeId)
        {
            PathBandwidth ins = BaseDBModel.GetById<PathBandwidth>(dbMgr, nodeId);
            ins.NodeId = nodeId;
            return ins;
        }

        static public Dictionary<int, PathBandwidth> Fetch(DBManager dbMgr)
        {
            Dictionary<int, PathBandwidth> pathWidthDict = new Dictionary<int, PathBandwidth>();
            List<PathBandwidth> pathWidthList = BaseDBModel.Fetch<PathBandwidth>(dbMgr);
            foreach (PathBandwidth model in pathWidthList)
            {
                pathWidthDict[model.NodeId] = model;
            }
            return pathWidthDict;
        }
    }
}
