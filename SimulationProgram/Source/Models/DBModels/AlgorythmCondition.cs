﻿using MyDBLibrary;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    // アルゴリズム条件モデル.
    public class AlgorithmCondition : BaseDBModel
    {
        public enum AlgorithmGroup
        {
            Random,
            PathPriority,
            AdditionalHopCount,
            LinkUsage,
            PlacedPosition
        }

        public enum ChoiceLinkAlgorythm
        {
            Smallest,
            Middle,
            Largest
        }

        public const int MAX_ADDITIONAL_HOP_COUNT = 3;

        private new static string tableName = String.Format("algorithm_condition_{0}", DateTime.Now.ToString("yyyyMMdd"));

        private IntField groupId = new IntField("group_id");
        private TextField groupName = new TextField("group_name");
        private IntField algorithmId = new IntField("algorithm_id");
        private TextField algorithmName = new TextField("algorithm_name");
        private IntField bandwidth = new IntField("bandwidth");
        private ListField conditions = new ListField("conditions");


        public AlgorithmCondition(DBManager dbMgr=null) : base(dbMgr)
        {
            fields.Add(groupId);
            fields.Add(groupName);
            fields.Add(algorithmId);
            fields.Add(algorithmName);
            fields.Add(bandwidth);
            fields.Add(conditions);
            List<int> init = new List<int>();
            for (int i = 0; i < SimulationData.JPN25_NODE_NUM; i++)
            {
                init.Add(0);
            }
            Conditions = init;
        }
        
        // テーブル名プロパティ.
        public override string TableName
        {
            get { return tableName; }
        }

        // 静的なテーブル名を取得. 
        public static string GetTableName()
        {
            return tableName;
        }

        public int GroupId
        {
            set { groupId.Value = value; }
            get { return groupId.Value; }
        }

        public string GroupName
        {
            set { groupName.Value = value; }
            get { return groupName.Value; }
        }

        public int AlgorithmId
        {
            set { algorithmId.Value = value; }
            get { return algorithmId.Value; }
        }

        public string AlgorithmName
        {
            set { algorithmName.Value = value; }
            get { return algorithmName.Value; }
        }

        public int Bandwidth
        {
            set { bandwidth.Value = value; }
            get { return bandwidth.Value; }
        }

        public List<int> Conditions
        {
            set { ListField.SetIntList(conditions, value); }
            get { return ListField.GetIntList(conditions); }
        }

        public void SetHopCount(int nodeId, int hopCnt)
        {
            conditions[nodeId-1] = hopCnt.ToString();
        }

        public int GetHopCount(int nodeId)
        {
            return int.Parse(conditions[nodeId-1]);
        }

        public static AlgorithmCondition Get(DBManager dbMgr, int groupId, int algorithmId)
        {
            Dictionary<string, object> where = new Dictionary<string, object> { { "group_id", groupId }, { "algorithm_id", algorithmId } };
            AlgorithmCondition ins = BaseDBModel.Get<AlgorithmCondition>(dbMgr, where);
            ins.AlgorithmId = algorithmId;
            ins.GroupId = groupId;
            return ins;
        }

        // スタティックなデータなので一括で取得できるようにする.
        static public List<AlgorithmCondition> Fetch(DBManager dbMgr)
        {
            return BaseDBModel.Fetch<AlgorithmCondition>(dbMgr);
        }
    }
}
