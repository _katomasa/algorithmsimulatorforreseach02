﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    public class Path
    {
        private string id;
        private int node1;
        private int node2;
        private int bandWidth;
        private bool isFailed = false;

        private string bandwidthString = "";
        private string blankString = "";

        public Path(int node1, int node2, int bandWidth)
        {
            this.id = MakeCompositeID(node1, node2);
            this.node1 = node1;
            this.node2 = node2;
            this.bandWidth = bandWidth;
            for (int i = 0; i<bandWidth; i++)
            {
                bandwidthString += "1";
                blankString += "0";
            }
        }

        public int Node1
        {
            get { return node1; }
        }

        public int Node2
        {
            get { return node2; }
        }

        public string Id
        {
            set { id = value; }
            get { return id; }
        }

        public int BandWidth
        {
            set { bandWidth = value; }
            get { return bandWidth; }
        }

        public string BandWidthString
        {
            set { bandwidthString = value; }
            get { return bandwidthString; }
        }

        public string BlankString
        {
            set { blankString = value; }
            get { return blankString; }
        }

        public bool IsFailed
        {
            set { isFailed = value; }
            get { return isFailed; }
        }

        public void Reset()
        {
            isFailed = false;
        }

        public static string MakeCompositeID(int node1, int node2)
        {
            if (node1 < node2)
            {
                return string.Format("{0:D2}_{1:D2}", node1, node2);
            }
            return string.Format("{0:D2}_{1:D2}", node2, node1);
        }

        public string CheckIsFailedByString()
        {
            string result;
            if (isFailed){
                result = "配置失敗";
            }
            else
            {
                result = "配置成功";
            }
            return result;
        }
    }
}
