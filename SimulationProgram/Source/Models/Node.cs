﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    public class Node
    {
        private int id;
        private bool isPlaced;
        private List<List<AdjacentNode>> routeData = new List<List<AdjacentNode>>();

        private int adjacentNodeNum = 0;

        public Node(int id)
        {
            this.id = id;
        }

        public int Id
        {
            set { id = value; }
            get { return id; }
        }

        public bool IsPlaced
        {
            set { isPlaced = value; }
            get { return isPlaced; }
        }

        public List<List<AdjacentNode>> RouteData
        {
            set { routeData = value; }
            get { return routeData; }
        }

        public int AdjacentNodeNum
        {
            set { adjacentNodeNum = value; }
            get { return adjacentNodeNum; }
        }

        public void Reset()
        {
            isPlaced = false;
        }

        internal string Result()
        {
            throw new NotImplementedException();
        }
    }
}
