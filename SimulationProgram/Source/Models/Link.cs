﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulationProgram
{
    public class Link
    {
        private string id;
        private int node1;
        private int node2;
        private string linkCapacity;

        public Link(int node1, int node2, int capacity)
        {
            this.id = MakeCompositeID(node1, node2);
            this.node1 = node1;
            this.node2 = node2;

            for (int i=0; i<capacity; i++) { linkCapacity += "0"; }
        }

        public string Id
        {
            set { id = value; }
            get { return id; }
        }

        public int Capacity
        {
            get { return linkCapacity.Length; }
        }

        public int Usage
        {
            get { return linkCapacity.ToList().Where(c => c == '1').ToList().Count; }
        }

        public int Rest
        {
            get { return linkCapacity.ToList().Where(c => c == '0').ToList().Count; }
        }

        public int GetAnotherNode(int nodeId)
        {
            if (nodeId==node1)
            {
                return node2;
            }
            else
            {
                return node1;
            }
        }

        public static string MakeCompositeID(int node1, int node2)
        {
            if (node1 < node2)
            {
                return string.Format("{0:D2}_{1:D2}", node1, node2);
            }
            return string.Format("{0:D2}_{1:D2}", node2, node1);
        }

        public List<int> GetAvalablePositionList(Path path)
        {
            List<int> positionList = new List<int>();
            int index = 0;
            while(index + path.BandWidth < linkCapacity.Length)
            {
                index = linkCapacity.IndexOf(path.BlankString, index);
                if(index < 0) { break; }
                positionList.Add(index);
                index++;
            }
            return positionList;
        }

        // 光パスを配置可能かチェック.
        // positionListが空でない場合は、
        public bool CanSetPath(Path path, List<int> positionList)
        {
            List<int> copyPositionList = new List<int>();
            copyPositionList.AddRange(positionList);
            foreach (int position in copyPositionList)
            {
                if(linkCapacity.IndexOf(path.BlankString, position) < 0)
                {
                    positionList.Remove(position);
                }
            }
            return 0 < positionList.Count;
        }

        // 光パスを配置.
        // IndexOf並びにLastIndexOfで配置可能な位置を探して配置する.
        // 配置位置は引数positionの位置. position=-1の場合は最初のリンクと判断しsetPositionから配置位置を決定する.
        public int SetPath(Path path, int position)
        {
            List<char> linkCapacityList = linkCapacity.ToList();
            for (int i = position; i<position+path.BandWidth; i++) { linkCapacityList[i]='1'; }
            linkCapacity = new String(linkCapacityList.ToArray());
            return position;
        }

        public void Reset()
        {
            int capacity = linkCapacity.ToList().Count;
            linkCapacity = "";
            for (int i = 0; i<capacity; i++) { linkCapacity += "0"; }
        }
    }
}
